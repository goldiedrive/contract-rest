package rest.goldie.drive.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Feign;
import feign.Logger;
import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import rest.goldie.drive.api.AuthApi;
import rest.goldie.drive.api.FileApi;
import rest.goldie.drive.api.ProfilerApi;
import rest.goldie.drive.api.RegistrationApi;
import rest.goldie.drive.tools.ByteArrayDecoder;

/**
 * Конфигурация Feign-клиентов для межсервисного взаимодействия по REST-API.
 */
@Configuration
@RequiredArgsConstructor
public class FeignClientsConfig {
    private final ObjectMapper objectMapper;
    private final ObjectFactory<HttpMessageConverters> messageConverters;

    /**
     * Бин для FormEncoder.
     *
     * @return Encoder
     */
    @Bean
    public Encoder feignFormEncoder() {
        return new SpringFormEncoder(new SpringEncoder(messageConverters));
    }

    /**
     * Бин Feign-клиента для сервиса Registration.
     * <p/>
     * Для активации необходимо добавить параметры в application.yml сервиса,
     * где планируется использовать клиент:
     * <pre>{@code
     * microservice:
     *   registration:
     *     url: http://goldie.drive:9090
     *     enable-feign-client: true
     * }</pre>
     *
     * @param baseUrl базовый URL-адрес сервиса.
     * @return Сконфигурированный клиент
     */
    @Bean
    @ConditionalOnProperty(
            value = "microservice.registration.enable-feign-client",
            havingValue = "true"
    )
    public RegistrationApi registrationApi(
            @Value("${microservice.registration.url}") String baseUrl
    ) {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new JacksonEncoder(objectMapper))
                .decoder(new JacksonDecoder(objectMapper))
                .logger(new Slf4jLogger(RegistrationApi.class))
                .logLevel(Logger.Level.NONE)
                .target(RegistrationApi.class, baseUrl);
    }

    /**
     * Бин Feign-клиента для сервиса Authorization.
     * <p/>
     * Для активации необходимо добавить параметры в application.yml сервиса,
     * где планируется использовать клиент:
     * <pre>{@code
     * microservice:
     *   authorization:
     *     url: http://goldie.drive:8081
     *     enable-feign-client: true
     * }</pre>
     *
     * @param baseUrl базовый URL-адрес сервиса.
     * @return Сконфигурированный клиент
     */
    @Bean
    @ConditionalOnProperty(
        value = "microservice.authorization.enable-feign-client",
        havingValue = "true"
    )
    public AuthApi authorizationApi(
        @Value("${microservice.authorization.url}") String baseUrl
    ) {
        return Feign.builder()
            .client(new OkHttpClient())
            .encoder(new JacksonEncoder(objectMapper))
            .decoder(new JacksonDecoder(objectMapper))
            .logger(new Slf4jLogger(AuthApi.class))
            .logLevel(Logger.Level.NONE)
            .target(AuthApi.class, baseUrl);
    }

    /**
     * Бин Feign-клиента для сервиса Profiler.
     * <p/>
     * Для активации необходимо добавить параметры в application.yml сервиса,
     * где планируется использовать клиент:
     * <pre>{@code
     * microservice:
     *   profiler:
     *     url: http://goldie.drive:8083
     *     enable-feign-client: true
     * }</pre>
     *
     * @param baseUrl базовый URL-адрес сервиса.
     * @return Сконфигурированный клиент
     */
    @Bean
    @ConditionalOnProperty(
        value = "microservice.profiler.enable-feign-client",
        havingValue = "true"
    )
    public ProfilerApi profilerApi(
        @Value("${microservice.profiler.url}") String baseUrl
    ) {
        return Feign.builder()
            .client(new OkHttpClient())
            .encoder(new JacksonEncoder(objectMapper))
            .decoder(new JacksonDecoder(objectMapper))
            .logger(new Slf4jLogger(ProfilerApi.class))
            .logLevel(Logger.Level.NONE)
            .target(ProfilerApi.class, baseUrl);
    }

    /**
     * Бин Feign-клиента для сервиса File.
     * <p/>
     * Для активации необходимо добавить параметры в application.yml сервиса,
     * где планируется использовать клиент:
     * <pre>{@code
     * microservice:
     *   file-service:
     *     url: http://goldie.drive:8086
     *     enable-feign-client: true
     * }</pre>
     *
     * @param baseUrl базовый URL-адрес сервиса.
     * @return Сконфигурированный клиент
     */
    @Bean
    @ConditionalOnProperty(
        value = "microservice.file-service.enable-feign-client",
        havingValue = "true"
    )
    public FileApi fileApi(
        @Value("${microservice.file-service.url}") String baseUrl
    ) {
        return Feign.builder()
            .client(new OkHttpClient())
            .encoder(feignFormEncoder())
            .decoder(new ByteArrayDecoder(objectMapper))
            .logger(new Slf4jLogger(FileApi.class))
            .logLevel(Logger.Level.NONE)
            .target(FileApi.class, baseUrl);
    }
}
