package rest.goldie.drive.api;

import feign.Headers;
import feign.RequestLine;
import rest.goldie.drive.model.UserDataTo;

/**
 * REST-API сервиса Profiler.
 */
@Headers({
    "Accept: application/json",
    "Content-Type: application/json"
})
public interface ProfilerApi {

    @RequestLine("POST /profiler/createUser")
    UserDataTo saveUserData(UserDataTo userDataTo);
}