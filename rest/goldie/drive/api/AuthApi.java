package rest.goldie.drive.api;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import rest.goldie.drive.model.User;
import rest.goldie.drive.model.enums.Answer;

import java.util.List;

/**
 * REST-API сервиса Authorization.
 */
@Headers({
    "Accept: application/json",
    "Content-Type: application/json"
})
public interface AuthApi {

    @RequestLine("GET /authorization/isValidJwtToken?token={token}&email={email}")
    Answer isValidJwtToken(@Param("token") String token, @Param("email") String email);

    @RequestLine("GET /authorization/extractUserName?token={token}")
    User extractUserName(@Param("token") String token);

    @RequestLine("GET /authorization/extractUserAuthorities?token={token}")
    List<String> extractUserAuthorities(@Param("token") String token);
}
