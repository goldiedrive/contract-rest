package rest.goldie.drive.api;

import feign.FeignException;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;
import rest.goldie.drive.model.file.File;

/**
 * REST API сервиса File.
 */
public interface FileApi {

    /**
     * Метод получения метаданных файла по его имени.
     *
     * @param fileName имя файла
     * @return данные о файле
     * @throws FeignException.NotFound если файл не найден
     */
    @RequestLine("GET /file-service/{fileName}/info")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    File getInfoByName(@Param String fileName);

    /**
     * Метод получения содержимого файла по его имени.
     *
     * @param fileName имя файла
     * @return содержимое файла
     * @throws FeignException.NotFound если файл не найден
     */
    @RequestLine("GET /file-service/{fileName}")
    ByteArrayResource getContentByName(@Param String fileName);

    /**
     * Метод проверки наличия файла среди ранее загружаемых.
     *
     * @param file файл
     * @return true, если файл найден, false иначе
     */
    @RequestLine("POST /file-service/check-exists")
    @Headers("Content-Type: multipart/form-data; "
            + " boundary=---boundary;")
    Boolean checkIsExists(@Param("file") MultipartFile file);

    /**
     * Метод загрузки файлов.
     *
     * @param file имя файла
     * @return мета данные файла
     */
    @RequestLine("POST /file-service/upload")
    @Headers("Content-Type: multipart/form-data; "
        + " boundary=---boundary;")
    File uploadFile(@Param("file") MultipartFile file);

    /**
     * Метод удаления файла.
     *
     * @param fileName имя файла
     */
    @RequestLine("DELETE /file-service/delete/{fileName}")
    void removeFile(@Param("fileName") String fileName);
}
