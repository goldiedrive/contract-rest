package rest.goldie.drive.api;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import rest.goldie.drive.model.UserSignInRequest;
import rest.goldie.drive.model.enums.Answer;
import rest.goldie.drive.model.User;

/**
 * REST-API сервиса Registration.
 */
@Headers({
        "Accept: application/json",
        "Content-Type: application/json"
})
public interface RegistrationApi {
    /**
     * Существует ли email.
     *
     * @param email почтовый адрес.
     * @return yes/not.
     */
    @RequestLine("GET /registration/isUserExist?email={email}")
    Answer isUserExist(@Param("email") String email);

    /**
     * Изменение пароля.
     *
     * @param userSignInRequest почтовый адрес, пароль.
     * @return ok, либо кидает ошибку
     */
    @RequestLine("POST /registration/feign/changePassword")
    Answer changePassword(UserSignInRequest userSignInRequest);

    /**
     * Проверка валидности пароля.
     *
     * @param userSignInRequest почтовый адрес, пароль.
     * @return yes/not.
     */
    @RequestLine("POST /registration/feign/isValidPassword")
    Answer isValidPassword(UserSignInRequest userSignInRequest);

    /**
     * Получение информации о пользователе: email, password, role, locked, enable.
     *
     * @param email почтовый адрес.
     * @return User.
     */
    @RequestLine("GET /registration/feign/getUser?email={email}")
    User getUser(@Param("email") String email);
}
