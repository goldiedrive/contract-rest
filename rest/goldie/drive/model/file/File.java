package rest.goldie.drive.model.file;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Файл.
 *
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class File {

    /**
     * Оригинальное наименование.
     * Example: image.png
     */
    private String originalName;

    /**
     * Content type.
     * Example: plain/text
     */
    private String contentType;

    /**
     * Внутренняя ссылка.
     * Example: d28977b03ff64fd58679f8d0c89a2435_image.png
     */
    private String internalLink;

    /**
     * Контрольная сумма файла (MD5).
     */
    private String md5Checksum;

    /**
     * Содержимое в виде массива байтов.
     */
    private byte[] content;
}
