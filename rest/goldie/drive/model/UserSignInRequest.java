package rest.goldie.drive.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserSignInRequest {
    private String email;
    private String password;
}
