package rest.goldie.drive.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Answer {
    OK("ok"),
    YES("yes"),
    NOT("not");

    private final String value;
}
