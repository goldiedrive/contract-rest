package rest.goldie.drive.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum UserRoles {
    STUDENT("STUDENT"),
    MASTER("MASTER"),
    ADMIN("ADMIN");

    private final String value;
}
