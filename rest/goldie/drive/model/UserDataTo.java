package rest.goldie.drive.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import rest.goldie.drive.model.enums.UserRoles;

@AllArgsConstructor
@Data
@Accessors(chain = true)
public class UserDataTo {
    private String email;
    private String lastName;
    private String firstName;
    private String patronymic;
    private String phoneNumber;
    private UserRoles role;
}
