package rest.goldie.drive.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import rest.goldie.drive.model.enums.UserRoles;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class User {
    private String email;
    private String password;
    private UserRoles userRole;
    private Boolean locked;
    private Boolean enabled;
}
